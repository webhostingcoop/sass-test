#!/bin/bash

if [[ -d bulma-test ]]; then
  cd /tmp/bulma-test
else
  gatsby new bulma-test https://github.com/amandeepmittal/gatsby-bulma-quickstart
  cd bulma-test
fi

cat << EOF > ./src/components/style.sass
\$font-stack: helvetica, sans-serif
\$primary-color: #333

body
  font: 100% \$font-stack
  color: \$primary-color
EOF

sed -i 's/style\.scss/style.sass/g' ./src/components/layout.js

gatsby develop
