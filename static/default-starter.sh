#!/bin/bash
: ${TARGET:=sass-test}
: ${STARTER:=https://github.com/gatsbyjs/gatsby-starter-default}

if [[ -d $TARGET ]]; then
  cd /tmp/$TARGET
else
  gatsby new $TARGET $STARTER
  cd $TARGET
  #npm install node-sass gatsby-plugin-sass
  yarn add node-sass gatsby-plugin-sass
fi

cat << EOF > ./src/components/sassy.sass
\$font-stack: helvetica, sans-serif
\$primary-color: #333

body
  font: 100% \$font-stack
  color: \$primary-color
EOF

sed -i 's/layout\.css/sassy.sass/g' ./src/components/layout.js

gatsby develop
